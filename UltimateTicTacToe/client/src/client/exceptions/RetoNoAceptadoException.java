package client.exceptions;

public class RetoNoAceptadoException extends Exception {
	 public RetoNoAceptadoException() {
		super("Reto no aceptado");
	}
}